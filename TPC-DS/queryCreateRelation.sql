--store_sales
--ss_promotion
CREATE TABLE ss_promotion as SELECT store_sk as ':START_ID(Store)', store_sk, promo_sk,
sum(net_profit) as net_profit, min(sold_date_sk) as start_valid_time,
max(sold_date_sk) as end_valid_time, promo_sk as ':END_ID(Promotion)'
FROM store_sales
GROUP BY store_sk, promo_sk;
ALTER TABLE ss_promotion ADD COLUMN ':TYPE' char(25);
UPDATE ss_promotion SET ':TYPE'='SS_Promotion';

--ss_customer
CREATE TABLE ss_customer as SELECT store_sk as ':START_ID(Store)',store_sk,customer_sk,sum(net_profit) as net_profit,
sum(net_paid) as net_paid, count(item_sk) as 'number_of_buy_times' , min(sold_date_sk) as start_valid_time,
max(sold_date_sk) as end_valid_time, customer_sk as ':END_ID(Customer)'
FROM store_sales
GROUP BY store_sk, customer_sk;
ALTER TABLE ss_customer ADD COLUMN ':TYPE' char(25);
UPDATE ss_customer SET ':TYPE'='SS_Customer';

--ss_item
CREATE TABLE ss_item as SELECT store_sk as ':START_ID(Store)',store_sk,item_sk,
sum(net_profit) as net_profit, sum(quantity) as quantity,
avg(wholesale_cost) as wholesale, avg(sales_price) as sales_price,
min(sold_date_sk) as start_valid_time, max(sold_date_sk) as end_valid_time,
item_sk as ':END_ID(Item)'
FROM store_sales
GROUP BY store_sk, item_sk;
ALTER TABLE ss_item ADD COLUMN ':TYPE' char(25);
UPDATE ss_item SET ':TYPE'='SS_Item';


--ss_customer_item
CREATE TABLE ss_customer_item as SELECT customer_sk as ':START_ID(Customer)',customer_sk,item_sk,
sum(net_paid) as net_paid, count(item_sk) as 'number_of_buy_times',
min(sold_date_sk) as start_valid_time, max(sold_date_sk) as end_valid_time,
item_sk as ':END_ID(Item)'
FROM store_sales
GROUP BY customer_sk, item_sk;
ALTER TABLE ss_customer_item ADD COLUMN ':TYPE' char(25);
UPDATE ss_customer_item SET ':TYPE'='SS_Customer_Item';


--store_returns
--sr_customer
CREATE TABLE sr_customer as SELECT store_sk as ':START_ID(Store)', store_sk, customer_sk,
count(item_sk) as 'number_of_return_times',
sum(refunded_cash) as refunded_cash, sum(return_amt) as return_amt,
sum(return_quantity) as return_quantity,
min(returned_date_sk) as start_valid_time, max(returned_date_sk) as end_valid_time,
customer_sk as ':END_ID(Customer)'
FROM store_returns
GROUP BY customer_sk, store_sk;
ALTER TABLE sr_customer ADD COLUMN ':TYPE' char(25);
UPDATE sr_customer SET ':TYPE'='SR_Customer';

--sr_item
CREATE TABLE sr_item as SELECT store_sk as ':START_ID(Store)', store_sk,item_sk,
avg(return_ship_cost) as return_ship_cost,
sum(refunded_cash) as refunded_cash,
sum(return_amt) as return_amt, sum(return_quantity) as return_quantity,
sum(store_credit) as store_credit, sum(net_loss) as net_loss,
min(returned_date_sk) as start_valid_time, max(returned_date_sk) as end_valid_time,
item_sk as ':END_ID(Item)'
FROM store_returns
GROUP BY store_sk, item_sk;
ALTER TABLE sr_item ADD COLUMN ':TYPE' char(25);
UPDATE sr_item SET ':TYPE'='SR_Item';


--sr_customer_item
CREATE TABLE sr_customer_item as SELECT customer_sk as ':START_ID(Customer)', customer_sk,item_sk,
count(item_sk) as 'number_of_return_times',sum(refunded_cash) as refunded_cash,
sum(return_amt) as return_amt, sum(return_quantity) as return_quantity,
min(returned_date_sk) as start_valid_time, max(returned_date_sk) as end_valid_time,
item_sk as ':END_ID(Item)'
FROM store_returns
GROUP BY customer_sk, item_sk;
ALTER TABLE sr_customer_item ADD COLUMN ':TYPE' char(25);
UPDATE sr_customer_item SET ':TYPE'='SR_Customer_Item';

--web_sales
--ws_customer
CREATE TABLE ws_customer as SELECT web_page_sk as ':START_ID(Web_Page)',
web_page_sk, bill_customer_sk,
count(item_sk) as 'number_of_buy_times', sum(net_paid) as net_paid,
sum(net_profit) as net_profit, sum(quantity) as quantity,
min(sold_date_sk) as start_valid_time, max(sold_date_sk) as end_valid_time,
bill_customer_sk as ':END_ID(Customer)'
FROM web_sales
GROUP BY web_page_sk, bill_customer_sk;
ALTER TABLE ws_customer ADD COLUMN ':TYPE' char(25);
UPDATE ws_customer SET ':TYPE'='WS_Customer';

--ws_item
CREATE TABLE ws_item as SELECT web_page_sk as ':START_ID(Web_Page)',
web_page_sk, item_sk,
sum(quantity) as 'quantity', avg(wholesale_cost) as wholesale_cost,
avg(sales_price) as sales_price, sum(net_profit) as net_profit,
min(sold_date_sk) as start_valid_time, max(sold_date_sk) as end_valid_time,
item_sk as ':END_ID(Item)'
FROM web_sales
GROUP BY web_page_sk, item_sk;
ALTER TABLE ws_item ADD COLUMN ':TYPE' char(25);
UPDATE ws_item SET ':TYPE'='WS_Item';

--ws_promotion
CREATE TABLE ws_promotion as SELECT web_page_sk as ':START_ID(Web_Page)',
web_page_sk,promo_sk,
sum(net_profit) as net_profit, min(sold_date_sk) as start_valid_time,
max(sold_date_sk) as end_valid_time, promo_sk as ':END_ID(Promotion)'
FROM web_sales
GROUP BY web_page_sk, promo_sk;
ALTER TABLE ws_promotion ADD COLUMN ':TYPE' char(25);
UPDATE ws_promotion SET ':TYPE'='WS_Promotion';


--ws_web_site_web_page
CREATE TABLE ws_web_site_web_page as SELECT web_page_sk as ':START_ID(Web_Page)',
web_page_sk, web_site_sk,
min(sold_date_sk) as start_valid_time, max(sold_date_sk) as end_valid_time,
web_site_sk as ':END_ID(Web_Site)'
FROM web_sales
GROUP BY web_page_sk, web_site_sk;
ALTER TABLE ws_web_site_web_page ADD COLUMN ':TYPE' char(25);
UPDATE ws_web_site_web_page SET ':TYPE'='WS_Web_Site_Web_Page';

--ws_customer_item
CREATE TABLE ws_customer_item as SELECT bill_customer_sk as ':START_ID(Customer)',
bill_customer_sk, item_sk,
sum(quantity) as 'quantity', sum(net_paid) as net_paid,
count(bill_customer_sk) as 'number_of_buy_times',
min(sold_date_sk) as start_valid_time, max(sold_date_sk) as end_valid_time,
item_sk as ':END_ID(Item)'
FROM web_sales
GROUP BY bill_customer_sk, item_sk;
ALTER TABLE ws_customer_item ADD COLUMN ':TYPE' char(25);
UPDATE ws_customer_item SET ':TYPE'='WS_Customer_Item';

--web_returns
--wr_customer
CREATE TABLE wr_customer as SELECT web_page_sk as ':START_ID(Web_Page)',
web_page_sk, refunded_customer_sk,
count(item_sk) as 'number_of_return_times',
sum(return_quantity) as return_quantity, sum(return_amt) as return_amt,
sum(refunded_cash) as refunded_cash,
min(returned_date_sk) as start_valid_time, max(returned_date_sk) as end_valid_time,
refunded_customer_sk as ':END_ID(Customer)'
FROM web_returns
GROUP BY web_page_sk, refunded_customer_sk;
ALTER TABLE wr_customer ADD COLUMN ':TYPE' char(25);
UPDATE ws_customer SET ':TYPE'='WR_Customer';


--wr_item
CREATE TABLE wr_item as SELECT web_page_sk as ':START_ID(Web_Page)',
web_page_sk, item_sk,
sum(return_ship_cost) as return_ship_cost,
sum(refunded_cash) as refunded_cash,
sum(account_credit) as account_credit,
sum(net_loss) as net_loss,
sum(return_quantity) as return_quantity, sum(return_amt) as return_amt,
min(returned_date_sk) as start_valid_time, max(returned_date_sk) as end_valid_time,
item_sk as ':END_ID(Item)'
FROM web_returns
GROUP BY web_page_sk, item_sk;
ALTER TABLE wr_item ADD COLUMN ':TYPE' char(25);
UPDATE wr_item SET ':TYPE'='WR_Item';

--wr_customer_item
CREATE TABLE wr_customer_item as SELECT refunded_customer_sk as ':START_ID(Customer)',
refunded_customer_sk, item_sk,
count(item_sk) as 'number_of_return_times',
sum(return_quantity) as return_quantity, sum(return_amt) as return_amt,
sum(refunded_cash) as refunded_cash,
min(returned_date_sk) as start_valid_time, max(returned_date_sk) as end_valid_time,
item_sk as ':END_ID(Item)'
FROM web_returns
GROUP BY web_page_sk, refunded_customer_sk;
ALTER TABLE wr_customer_item ADD COLUMN ':TYPE' char(25);
UPDATE wr_customer_item SET ':TYPE'='WR_Customer_Item';

--catalog_sales
--cs_customer
CREATE TABLE cs_customer as SELECT catalog_page_sk as ':START_ID(Catalog_Page)',
catalog_page_sk, bill_customer_sk,
count(item_sk) as 'number_of_buy_times', sum(net_paid) as net_paid,
min(sold_date_sk) as start_valid_time, max(sold_date_sk) as end_valid_time,
bill_customer_sk as ':END_ID(Customer)'
FROM catalog_sales
GROUP BY catalog_page_sk, bill_customer_sk;
ALTER TABLE cs_customer ADD COLUMN ':TYPE' char(25);
UPDATE cs_customer SET ':TYPE'='CS_Customer';

--cs_item
CREATE TABLE cs_item as SELECT catalog_page_sk as ':START_ID(Catalog_Page)',
catalog_page_sk, item_sk,
sum(quantity) as quantity, avg(wholesale_cost) as wholesale_cost,
avg(sales_price) as sales_price,
min(sold_date_sk) as start_valid_time, max(sold_date_sk) as end_valid_time,
item_sk as ':END_ID(Item)'
FROM catalog_sales
GROUP BY catalog_page_sk, item_sk;
ALTER TABLE cs_item ADD COLUMN ':TYPE' char(25);
UPDATE cs_item SET ':TYPE'='CS_Item';

--cs_call_center
CREATE TABLE cs_call_center as SELECT catalog_page_sk as ':START_ID(Catalog_Page)',
catalog_page_sk, call_center_sk,
sum(quantity) as quantity, avg(net_profit) as net_profit,
min(sold_date_sk) as start_valid_time, max(sold_date_sk) as end_valid_time,
call_center_sk as ':END_ID(Call_Center)'
FROM catalog_sales
GROUP BY catalog_page_sk, call_center_sk;
ALTER TABLE cs_call_center ADD COLUMN ':TYPE' char(25);
UPDATE cs_call_center SET ':TYPE'='CS_Call_Center';

--cs_customer_item
CREATE TABLE cs_customer_item as SELECT bill_customer_sk as ':START_ID(Customer)',
bill_customer_sk, item_sk,
count(item_sk) as 'number_of_buy_times', sum(net_paid) as net_paid,
min(sold_date_sk) as start_valid_time, max(sold_date_sk) as end_valid_time,
item_sk as ':END_ID(Item)'
FROM catalog_sales
GROUP BY catalog_page_sk, bill_customer_sk;
ALTER TABLE cs_customer_item ADD COLUMN ':TYPE' char(25);
UPDATE cs_customer_item SET ':TYPE'='CS_Customer_Item';

--cs_promotion
CREATE TABLE cs_promotion as SELECT catalog_page_sk as ':START_ID(Catalog_Page)',
catalog_page_sk, promo_sk,
count(item_sk) as 'number_of_buy_times', sum(net_paid) as net_paid,
min(sold_date_sk) as start_valid_time, max(sold_date_sk) as end_valid_time,
item_sk as ':END_ID(Item)'
FROM catalog_sales
GROUP BY catalog_page_sk, promo_sk;
ALTER TABLE cs_promotion ADD COLUMN ':TYPE' char(25);
UPDATE cs_promotion SET ':TYPE'='CS_Promotion';

--catalog_returns
--cr_customer
CREATE TABLE cr_customer as SELECT catalog_page_sk as ':START_ID(Catalog_Page)',
catalog_page_sk, refunded_customer_sk,
count(item_sk) as 'number_of_return_times', sum(return_quantity) as return_quantity,
sum(refunded_cash) as refunded_cash, sum(return_amount) as return_amount,
min(returned_date_sk) as start_valid_time, max(returned_date_sk) as end_valid_time,
refunded_customer_sk as ':END_ID(Customer)'
FROM catalog_returns
GROUP BY catalog_page_sk, refunded_customer_sk;
ALTER TABLE cr_customer ADD COLUMN ':TYPE' char(25);
UPDATE cr_customer SET ':TYPE'='CR_Customer';


--cr_item
CREATE TABLE cr_item as SELECT catalog_page_sk as ':START_ID(Catalog_Page)',
catalog_page_sk, item_sk,
sum(return_ship_cost) as return_ship_cost,sum(refunded_cash) as refunded_cash,
sum(store_credit) as store_credit,sum(net_loss) as net_loss,
sum(return_quantity) as return_quantity, sum(return_amount) as return_amount,
min(returned_date_sk) as start_valid_time, max(returned_date_sk) as end_valid_time,
item_sk as ':END_ID(Item)'
FROM catalog_returns
GROUP BY catalog_page_sk, refunded_customer_sk;
ALTER TABLE cr_item ADD COLUMN ':TYPE' char(25);
UPDATE cr_item SET ':TYPE'='CR_Item';

--cr_customer_item
CREATE TABLE cr_customer_item as SELECT refunded_customer_sk as ':START_ID(Customer)',
refunded_customer_sk,item_sk,
count(item_sk) as 'number_of_return_times', sum(return_quantity) as return_quantity,
sum(refunded_cash) as refunded_cash, sum(return_amount) as return_amount,
min(returned_date_sk) as start_valid_time, max(returned_date_sk) as end_valid_time,
item_sk as ':END_ID(Item)'
FROM catalog_returns
GROUP BY catalog_page_sk, refunded_customer_sk;
ALTER TABLE cr_customer_item ADD COLUMN ':TYPE' char(25);
UPDATE cr_customer_item SET ':TYPE'='CR_Customer_Item';

--cr_call_center
CREATE TABLE cr_call_center as SELECT catalog_page_sk as ':START_ID(Catalog_Page)',
catalog_page_sk, call_center_sk,
sum(net_loss) as net_loss, sum(return_quantity) as return_quantity,
min(returned_date_sk) as start_valid_time, max(returned_date_sk) as end_valid_time,
call_center_sk as ':END_ID(Call_Center)'
FROM catalog_returns
GROUP BY catalog_page_sk, call_center_sk;
ALTER TABLE cr_call_center ADD COLUMN ':TYPE' char(25);
UPDATE cr_call_center SET ':TYPE'='CR_Call_Center';
