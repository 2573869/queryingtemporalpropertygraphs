# QueryingTemporalPropertyGraphs

**Experimental evaluation in the article "Querying temporal property graphs" (CAISE 2022)**

***Abstract of the paper***

Nowadays, in many real-world problems, objects are characterized by properties and interactions that can evolve over time. Several temporal property graph models and their associated query languages are proposed in the literature. However, they are not concerned with widespread use. To overcome this drawback, we propose a conceptual query language which allows querying temporal property graphs in a declarative way and independently from any application domain and implementation environment. More specifically, we propose operators to express queries with conditions on time and evolution. We also define translation rules between our operators and existing property graph query languages to implement them easily. To illustrate the power of our solution, we present two case studies based on a Neo4j and an OrientDB implementations of our operators and show some real-world querying examples.

Keywords: Operators, Temporal evolution, Implementation, Neo4j, OrientDB


***Summary of the experimental evaluation***

We run an experiment with the two following objectives: (i) to illustrate the feasibility of our operators by translating them into graph queries adapted to different implementation environments; (ii) to record query execution times without an optimized configuration in order to see if they are reasonable;

We have identified the relevant graph databases to show how to implement our operators easily. To do so, we use the following hardware configuration for the experiment: PowerEdge R630, 16 CPUs x Intel(R) Xeon(R) CPU E5-2630 v3 @ 2.40Ghz, 63.91 GB. Two virtual machines are installed on this hardware. Each virtual machine has 6GB in terms of RAM and 100GB in terms of disk size. On each of the two virtual machines, we installed respectively a graph database compatible with the property graph model: (i) Neo4j (version 4.1.3) and (ii) OrientDB (version 3.0.4).  



***Neo4j-based implementation***


--- Technical environment ---

On one of the virtual machine, we installed the database management system Neo4j Server (the version 4.1.3 and the edition Community) as well as the IDE Pycharm (version 2020.1.2).

Install your technical environment:

Install Neo4j Server.
Install Pycharm.
Open Pycharm.
Clone this remote repository in Pycharm. It will create a pycharm project.

---- Social experiment dataset ----

The Social experiment dataset has been collected from a social experiment on students from MIT who lived in dormitory from October 2008 to May 2009 . It is available online at [Reality Commons website](http://realitycommons.media.mit.edu/socialevolution.html). This experiment was designed to study the adoption of political opinions, diet, exercise, obesity, eating habits, epidemiological contagion, depression and stress, dorm political issues, interpersonal relationships, and privacy. It includes daily surveys about the flu symptoms of students: (1) runny nose, nasal congestion, and sneezing; (2) nausea, vomiting, and diarrhea; (3) frequent stress; (4) sadness and depression; and (5) fever. Moreover, it includes physical information about proximity, location, call and Wi-Fi access points logs of the students every six minutes. Finally, it includes information about relationships between students (close friends, socialize twice per week, political discussant, share facebook and twitter activities). This dataset includes changes over time: (i) on the value of the symptoms of students and (ii) on the interactions between students. We used this dataset to answer the objective of evaluating the scalability of our model. To do so, we transformed the Social experiment dataset into our temporal graph representation. 

We can find in the folder [socialexperiment](https://gitlab.com/2573869/queryingtemporalpropertygraphs/-/tree/main/socialexperiment) : 

- the translation of the original dataset into a temporal graph schema representation in the file [Social_experiment_documentation_data_cleaning_transformation.pdf](https://gitlab.com/2573869/queryingtemporalpropertygraphs/-/blob/main/socialexperiment/Social_experiment_documentation_data_cleaning_transformation.pdf);
- the processes to clean and transform the dataset into our temporal graph representation in the file  [Social_experiment_documentation_data_cleaning_transformation.pdf](https://gitlab.com/2573869/queryingtemporalpropertygraphs/-/blob/main/socialexperiment/Social_experiment_documentation_data_cleaning_transformation.pdf);
- the python programs used in the processes to clean the dataset (i.e. treat incorrect and null values) in the folder [Datacleaning](https://gitlab.com/2573869/queryingtemporalpropertygraphs/-/tree/main/socialexperiment/dataCleaning);
- the python programs used in the processes to transform the dataset into our temporal graph representation in the folder [transformation](https://gitlab.com/2573869/queryingtemporalpropertygraphs/-/tree/main/socialexperiment/transformation); 


---- Donwload the transformed dataset ----

The dataset obtained as a result of the transformation process is named final dataset at the following link: https://cloud.irit.fr/index.php/s/ZQkToXDkaQyJmmV 

Download and save the dataset in a local directory.

---- Import the dataset in Neo4j ----

We can find in the folder [importation](https://gitlab.com/2573869/queryingtemporalpropertygraphs/-/tree/main/socialexperiment/importation) the python program to import the dataset into Neo4j ;

---- Query the dataset in Neo4j ----

We can find in the folder [query](https://gitlab.com/2573869/queryingtemporalpropertygraphs/-/tree/main/socialexperiment/query):
- the csv file of the queries we run on Social experiment dataset ;
- the python program to query the dataset into Neo4j based on queries on the csv file.


***OrientDB-based implementation***

--- Technical environment ---

On one of the virtual machine, we installed the database management system OrientDB (version 3.0.4).
Install your technical environment:

Install OrientDB.
Install Pycharm.
Open Pycharm.
Clone this remote repository in Pycharm. It will create a pycharm project.

---- TPC-DS dataset ----

Temporal evolutions exist in a reference benchmark available online, namely [TPC-DS benchmark](http://www.tpc.org/tpc_documents_current_versions/pdf/tpc-ds_v2.13.0.pdf). This benchmark is based on transaction data of a retail company. It allows us to find all the three types of evolution: (i) attribute value, (ii) attribute set and (iii) topology.

1) The [TPC-DS benchmark](http://www.tpc.org/tpcds/) allows to generate datasets according to parameterized data volumes. 
- get the generator toolkit : download it from the [TPC-DS website](http://tpc.org/tpc_documents_current_versions/download_programs/tools-download-request5.asp?bm_type=TPC-DS&bm_vers=2.13.0&mode=CURRENT-ONLY) or download it from the directory [TPC-DS](https://gitlab.com/2573869/tgmsq/-/tree/3aff15b2eba8751869fdb4d878d24e5effe5ce28/TPC-DS) of this project.
- follow the guidelines to use the the generator toolkit [here](https://datacadamia.com/data/type/relation/benchmark/tpcds/load#data_generation). We generated a dataset according to 4 scale factor to generate 3.7GB data.

2) We transformed the TPC-DS dataset into our temporal graph representation using python programs (.py) in this project.  We followed the transformation process presented at the file [data_transformation.png](https://gitlab.com/2573869/queryingtemporalpropertygraphs/-/blob/main/TPC-DS/data_transformation.PNG). This file specifies which python program to run at each step. 

N.B: After transformation, we made an additional modification on the file has_household_demographics.csv for each dataset due to an error we made. We used the python  program [modify_hhd.py](https://gitlab.com/2573869/queryingtemporalpropertygraphs/-/blob/main/TPC-DS/modify_hhd.py).

---- Donwload the transformed dataset ----

Download the transformed dataset [here](https://cloud.irit.fr/index.php/s/9g4OB35SoxwgEG6) and save the datasets in a local directory.
 

 ---- Import the dataset in OrientDB ----

Download the file [importOrientDB.zip](https://gitlab.com/2573869/queryingtemporalpropertygraphs/-/blob/main/TPC-DS/importOrientDB.zip) and follow the instructions in the file importOrientDB.txt.


---- Query the dataset ----
 

-  The csv file  [queryoperators4gb.csv](https://gitlab.com/2573869/queryingtemporalpropertygraphs/-/blob/main/TPC-DS/queryoperators4gb.csv) of the presents the query we run on TPC-DS dataset ;
- Run the python program  [queryOrientDB.py](https://gitlab.com/2573869/queryingtemporalpropertygraphs/-/blob/main/TPC-DS/queryOrientDB.py) to query the dataset into OrientDB based on the csv file.

 
