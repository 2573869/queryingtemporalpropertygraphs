# Pycharm IDE
# This is the program to import the dataset social experiment .


import os


# remove data in the import folder in Neo4j configuration files
# in our virtual machine, the path of the import folder is /var/lib/neo4j/import
# if you are not on the same environment than us, please verify the location of the import folder at https://neo4j.com/docs/operations-manual/current/configuration/file-locations/
passWord ='*******' # enter the password of your user account to authorize a linux command system in the virtual machine
cmd = 'sudo rm /var/lib/neo4j/import/*.csv'

# os.system(cmd) execute the command as in the terminal
# enter the password automatically
os.system('echo %s|sudo -S %s' % (passWord, cmd))

# import csv files from the folder containing the dataset to the import file of Neo4j
# in the first argument of cmd, specify the folder in which the dataset for snapshots are and add /*.csv
cmd = 'sudo cp /home/activus01/data/socialEvolution/*.csv /var/lib/neo4j/import'
os.system('echo %s|sudo -S %s' % (passWord, cmd))

# clean database dedicated for the dataset of socialevolution
# here our database is called socialevolution
cmd = 'sudo rm /var/lib/neo4j/data/databases/socialevolution.db/*'
os.system('echo %s|sudo -S %s' % (passWord, cmd))

# stop neo4j server
cmd = 'cd /var/lib/neo4j/'
os.system(cmd)
cmd = 'sudo neo4j stop'
os.system('echo %s|sudo -S %s' % (passWord, cmd))


# import data
cmd = 'cd /var/lib/neo4j/'
os.system(cmd)
cmd = 'sudo neo4j-admin import --database=socialevolution.db ' \
      '--nodes=/var/lib/neo4j/import/Student.csv ' \
      '--nodes=/var/lib/neo4j/import/WLAN.csv ' \
      '--relationships=/var/lib/neo4j/import/Access.csv ' \
      '--relationships=/var/lib/neo4j/import/Call.csv ' \
      '--relationships=/var/lib/neo4j/import/ReceiveSMS.csv ' \
      '--relationships=/var/lib/neo4j/import/SendSMS.csv ' \
      '--relationships=/var/lib/neo4j/import/Proximity.csv ' \
      '--relationships=/var/lib/neo4j/import/BlogLivejournalTwitter.csv ' \
      '--relationships=/var/lib/neo4j/import/CloseFriend.csv ' \
      '--relationships=/var/lib/neo4j/import/FacebookAllTaggedPhotos.csv ' \
      '--relationships=/var/lib/neo4j/import/PoliticalDiscussant.csv ' \
      '--relationships=/var/lib/neo4j/import/SocializeTwicePerWeek.csv ' \
      '--skip-bad-relationships --skip-duplicate-nodes'


os.system('echo %s|sudo -S %s' % (passWord, cmd))

# start server
cmd = 'cd /var/lib/neo4j/'
os.system(cmd)
cmd = 'sudo neo4j start'
os.system('echo %s|sudo -S %s' % (passWord, cmd))
