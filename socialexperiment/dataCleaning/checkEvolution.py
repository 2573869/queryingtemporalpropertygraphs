# check evolution of the value of the attributes
import pandas as pd
import time
import math
from outils import sortByCol

st = time.time()
#path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/Proximity.csv"
path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/Access.csv"

data = pd.read_csv(path, encoding='utf-8')
# sort by user_id and remove user id and time
#data = sortByCol(data, ['user_id', 'remote_user_id_if_known', 'time'])
data = sortByCol(data, ['user_id', 'wireless_mac', 'time'])

# check if there is the evolution
# evolutions = []
# places=[]
# users = data['user_id'].unique()
# for user in users:
#     userChunk = data[data['user_id'].isin([user])]
#     removes = userChunk['remote_user_id_if_known'].unique()
#     for remove in removes:
#         couple = userChunk[userChunk['remote_user_id_if_known'].isin([remove])]
#         id = couple.index.tolist()[0]
#         v = couple.at[id, 'prob2']
#         if math.isnan(v):
#             continue
#         nb_f = couple[(couple.prob2 != v)].shape[0]
#         places.append(id)
#         evolutions.append(nb_f)

# for Access
evolutions = []
places=[]
users = data['user_id'].unique()
for user in users:
    userChunk = data[data['user_id'].isin([user])]
    wlans = userChunk['wireless_mac'].unique()

    for wlan in wlans:
        couples = userChunk[userChunk['wireless_mac'].isin([wlan])]
        times=couples['time'].unique()
        for time in times:
            triple=couples[couples['time'].isin([time])]

            lastid = triple.index.tolist()[0]
            v = triple.at[lastid, 'strength']
            if math.isnan(v):
                continue
            nb_f = triple[(triple.strength != v)].shape[0]
            places.append(lastid)
            evolutions.append(nb_f)
sum=0
for i in evolutions:
   sum=sum+i
print('nb evolution : ',sum)
et = time.time()
cost_time = et - st
print('cost time：{}seconds'.format(float('%.2f' % cost_time)))
