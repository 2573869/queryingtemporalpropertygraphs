# check whether the lines are duplicated
import pandas as pd
#path="C:/Users/carol/Downloads/SocialEvolution/FluSymptoms.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/Proximity.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/calls.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/SMS.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/Access.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/FluSymptoms_ts2_extracted.csv"
path="C:/Users/carol/Downloads/SocialEvolution/socialEvolution/finalData/Access.csv"

data=pd.read_csv(path,encoding='utf-8',dtype=str)


data_duplication=data[data.duplicated(keep=False)]
nb_duplication=len(data_duplication)
print("before dropping the duplication:")
print("number of duplication",round(nb_duplication/2))

# remove duplications
data.drop_duplicates(keep="first",inplace=True)


# recheck the duplications
data_duplication=data[data.duplicated(keep=False)]
nb_duplication=len(data_duplication)
print("after dropping the duplication:")
print("number of duplication",round(nb_duplication/2))
#data.drop_duplicates(subset=['time','user_id'], keep='first', inplace=True)


# save the file after removing the duplications
#15
#data.to_csv(path[:-13]+"intermediate_files/Proximity.csv", index=False, encoding='utf-8')
data.to_csv(path[:-4]+"ddd.csv", index=False, encoding='utf-8')

print("successfully save the files")