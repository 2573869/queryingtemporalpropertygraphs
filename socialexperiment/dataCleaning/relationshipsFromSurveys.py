# slip RelationshipsFromSurveys.csv into the different files according to the relationships
import pandas as pd
import time

st=time.time()
path = "C:/Users/carol/Downloads/SocialEvolution/RelationshipsFromSurveys.csv"
data = pd.read_csv(path, encoding='utf-8')
# rename the column with decimal point
data.rename(columns={'id.A': 'id_A','id.B':'id_B','survey.date':'survey_date'},inplace=True)
# remove the row where id_A=id_B
data = data[(data.id_A != data.id_B)]


# sort the file by the order of relationships
data.sort_values(by=['relationship'], axis=0, ascending=[True], inplace=True)
# reset index of dataframe
data.reset_index(drop=True, inplace=True)
# find all of relationships
relationships = data['relationship'].unique().tolist()
print(relationships)
print('nb of remationships in the file ', len(relationships))

# find index of different type of relationships
# split the file
indexs=[]
t=0
for relationship in relationships:
    index=(data[(data.relationship == relationship)].index.tolist()[-1])
    print(index)
    indexs.append(index)
    df_new=data[t:index+1]
    t=index+1
    df_new.to_csv(path[:-28]+"intermediate_files/"+str(relationship)+".csv", index=False, encoding='utf-8')

et = time.time() # the end time
cost_time = et - st
print('cost time：{}seconds'.format(float('%.2f' % cost_time)))