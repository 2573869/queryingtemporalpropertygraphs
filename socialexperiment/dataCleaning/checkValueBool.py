# check whether every values of attributes are equal 0 or 1
import pandas as pd
path="C:/Users/carol/Downloads/SocialEvolution/FluSymptoms.csv"
data=pd.read_csv(path,encoding='utf-8')
nb_sore_throat_cough=len(data[(data["sore.throat.cough"] != 0) & (data["sore.throat.cough"] != 1)].index.tolist())
nb_runnynose_congestion_sneezing=len(data[(data["runnynose.congestion.sneezing"] != 0) & (data["runnynose.congestion.sneezing"] != 1)].index.tolist())
nb_fever=len(data[(data["fever"] != 0) & (data["fever"] != 1)].index.tolist())
nb_nausea_vomiting_diarrhea=len(data[(data["nausea.vomiting.diarrhea"] != 0) & (data["nausea.vomiting.diarrhea"] != 1)].index.tolist())
nb_sad_depressed=len(data[(data["sad.depressed"] != 0) & (data["sad.depressed"] != 1)].index.tolist())
nb_open_stressed=len(data[(data["open.stressed"] != 0) & (data["open.stressed"] != 1)].index.tolist())
print("number of value isn't 0/1")
print("column sore_throat_cough: ",nb_sore_throat_cough)
print("column open_stressed: ",nb_open_stressed)
print("column sad_depressed: ",nb_sad_depressed)
print("column fever: ",nb_fever)
print("column nausea_vomiting_diarrhea: ",nb_nausea_vomiting_diarrhea)
print("column runnynose_congestion_sneezing: ",nb_runnynose_congestion_sneezing)

