# check the abnormal data
# for example : a user is proximate to himself
import pandas as pd
import time
st=time.time()
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/Proximity.csv"
path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/calls.csv"
path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/SMS.csv"

data=pd.read_csv(path,encoding='utf-8',dtype=str)
#data=data[(data.user_id!=data.remote_user_id_if_known)]
data=data[(data.user_id!=data.dest_user_id_if_known)]

# remove the rows where the duration is equal to 0
#data=data[(data.duration!='0')]


data.to_csv(path, index=False, encoding='utf-8')
et = time.time()
cost_time = et - st
print('cost time：{}seconds'.format(float('%.2f' % cost_time)))
