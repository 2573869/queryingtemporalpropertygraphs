# rename the name of column
import pandas as pd
#path="C:/Users/carol/Downloads/SocialEvolution/SMS.csv"
path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/FluSymptoms.csv"
data=pd.read_csv(path,encoding='unicode_escape',dtype=str)
# rename the column with decimal point
#data.rename(columns={'user.id': 'user_id','dest.user.id.if.known':'dest_user_id_if_known','dest.phone.hash':'dest_phone_hash'},inplace=True)
data.rename(columns={'sore.throat.cough': 'sore_throat_cough','runnynose.congestion.sneezing':'runnynose_congestion_sneezing','nausea.vomiting.diarrhea':'nausea_vomiting_diarrhea','sad.depressed':'sad_depressed','open.stressed':'open_stressed'},inplace=True)

data.to_csv(path, index=False, encoding='utf-8')