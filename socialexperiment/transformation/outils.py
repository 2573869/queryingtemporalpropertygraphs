
import time

import pandas as pd
import numpy as np

def timeStamp(timeNum):
    """
     Convert the time stamp to a time in normal format -millisecond
    :param timeNum: time stamp to be converted
    :type timeNum:int
    :return : time in normal format
    :rtype : str
    """
    time_array = time.localtime(timeNum)
    time_final = time.strftime("%Y-%m-%dT%H:%M:%S", time_array)
    return time_final

def timeStampNoHMS(timeNum):
    """
     Convert the time stamp to a time in normal format -millisecond
    :param timeNum: time stamp to be converted
    :type timeNum:int
    :return : time in normal format
    :rtype : str
    """
    time_array = time.localtime(timeNum)
    time_final = time.strftime("%Y-%m-%d", time_array)
    return time_final
def toTimestampNoMS(date):
    timeArray = time.strptime(date, "%Y-%m-%d %H:%M:%S")
    timestamp = round(time.mktime(timeArray))
    return timestamp

def toTimestampEndByD(date):
    timeArray = time.strptime(date, "%Y-%m-%d %H:%M:%S")
    print(timeArray)
    timestamp = round(time.mktime(timeArray))
    return timestamp

def toTimestamp(date):
    timeArray = time.strptime(date, "%Y-%m-%dT%H:%M:%S")
    timestamp = round(time.mktime(timeArray))
    return timestamp
def toTimestampNoHMS(date):
    timeArray = time.strptime(date, "%Y-%m-%d")
    timestamp = round(time.mktime(timeArray))
    return timestamp

def toNeo4jTime(date):
    date=date.replace(' ','T')
    #timeArray = time.strptime(date, "%Y-%m-%d %H:%M:%S")
    #timestamp = round(time.mktime(timeArray))
    #timeFinam=timeStamp(timestamp)
    return date

def convertNeo4jTime(df,col):
    for index,row in df.iterrows():
        df.at[index,col]=toNeo4jTime(df.at[index,col])
    return df

def convertTime(df_data: "the dataframe of data to be converted",indexs: 'the list of colomne name of timestamp') -> "generate a dataframe":
    """
    Convert the time stamp in the target file to a time in normal format and generate a new file
    :param path:the path of teh target file
    :type path: dataframe
    :param index:the list of the column name where the timestamp is located
    :type index: list
    :return: dataframe after the sort
    """
    for i in indexs :
        for index,row in df_data.iterrows(): #row!!!!!

            if df_data.loc[index, i] != "":
                df_data.loc[index, i] = timeStamp(int(df_data.loc[index, i]))
    return df_data


def convertTimeNoHMS(df_data: "the dataframe of data to be converted",indexs: 'the list of colomne name of timestamp') -> "generate a dataframe":
    """
    Convert the time stamp in the target file to a time in normal format and generate a new file
    :param path:the path of teh target file
    :type path: dataframe
    :param index:the list of the column name where the timestamp is located
    :type index: list
    :return: dataframe after the sort
    """
    for i in indexs :
        for index,row in df_data.iterrows(): #row!!!!!
            if df_data.loc[index, i] != "":
                df_data.loc[index, i] = timeStampNoHMS(int(df_data.loc[index, i]))
    return df_data
def sortByCol (data,cols):
    """
    sort the dataframe by column given
    :param path: the path of the file to be sorted
    :type path: str
    :param cols: the columns referenced to sort the file
    :type cols: list
    :return: a dataframe well sorted
    :rtype:dataframe
    """
    ascending=[True for i in range (0,len(cols))]
    # sort by column
    data.sort_values(by=cols, axis=0, ascending=ascending, inplace=True)
    # reset the index of each row
    data.reset_index(drop=True, inplace=True)
    return data

def splitString(time):
    time=time[:-9]
    return time

def extractDateToTimestamp(path):
    data=pd.read_csv(path,encoding='utf-8')
    for index,row in data.iterrows():
        data.at[index,'time']=splitString(data.at[index,'time'])
        data.at[index,'time']=toTimestampNoHMS(data.at[index,'time'])
    data.to_csv(path[:-4]+"_ts.csv", index=False, encoding='utf-8')




if __name__ == "__main__":
    path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/Access_ts_extracted_12aaaaaa.csv"
   # extractDateToTimestamp(path)
    #print(splitString("2009-01-16 15:37:00"))
    #print(toTimestampEndByD("2009-01-16 15:37:00"))
    #print(timeStamp(1231455600))
    #print(toNeo4jTime("2009-01-16 08:41:39"))
    data=pd.read_csv(path,encoding='utf-8')
    data=convertTime(data,['startvalidtime'])
    data.drop(['endvalidtime'], axis=1, inplace=True)
    endvalidtimes = data['startvalidtime'].copy(deep=True)

    # construct a dataframe of student
    intermediate = {'endvalidtime': endvalidtimes}
    df_intermediate = pd.DataFrame(intermediate)

    # concat of 2 dataframe
    data = pd.concat([data, df_intermediate], axis=1)

    data.to_csv(path[:-4] + "fin.csv", index=False, encoding='utf-8')