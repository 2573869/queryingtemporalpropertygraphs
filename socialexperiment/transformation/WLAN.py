# transformation for WLAN.csv
import pandas as pd
import time

st = time.time()

path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/WLAN.csv"
data = pd.read_csv(path, encoding='utf-8',dtype=str)
# add validtime and :LABEL
startvalidtime=["2009-01-09T00:00:00" for i in range(0,data.shape[0])]
endvalidtime=["2009-04-25T00:00:00" for i in range(0,data.shape[0])]
label=["WLAN" for i in range(0,data.shape[0])]
intermediate = {'startvalidtime':startvalidtime,'endvalidtime':endvalidtime,':LABEL': label}
df_intermediate = pd.DataFrame(intermediate)
data = pd.concat([data, df_intermediate], axis=1)

#rename wireless_mac:ID(WLAN)
data.rename(columns={'wireless_mac':'wireless_mac:ID(WLAN)'},inplace=True)
# save the file into a csv file
data.to_csv(path[:-27]+"socialEvolution/WLAN1.csv", index=False, encoding='utf-8')

et = time.time()
cost_time = et - st
print('cost time：{}seconds'.format(float('%.2f' % cost_time)))