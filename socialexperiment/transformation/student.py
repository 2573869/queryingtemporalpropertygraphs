# programme of transformation for the file FluSymptoms.csv
import pandas as pd
from pandas import DataFrame
from outils import toTimestampNoMS,sortByCol,convertTime,convertTimeNoHMS
# show all of the column
pd.set_option('display.max_columns', None)
# show all of the rows
# pd.set_option('display.max_rows', None)
# read the file
path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/FluSymptoms_ts_student.csv"
data = pd.read_csv(path, encoding='utf-8')

path_suject="C:/Users/carol/Downloads/SocialEvolution/Subjects.csv"
df_suject=pd.read_csv(path_suject, encoding='utf-8')


# rename the column which the name include decimal point
# data.rename(columns={'sore.throat.cough': 'sore_throat_cough','runnynose.congestion.sneezing':'runnynose_congestion_sneezing','nausea.vomiting.diarrhea':'nausea_vomiting_diarrhea','sad.depressed':'sad_depressed','open.stressed':'open_stressed'},inplace=True)
# print(data.head())

# convert the time to timestamp
# for index,row in data.iterrows():
#   data.at[index,'time']=toTimestampNoMS(row['time'])


# sorted by user_id and time
data=sortByCol(data,['user_id','time'])

# leave the rows where the change happened
# extract the header of file as header for the new file
header = data.columns.tolist()
widthDf = len(header)
# length of the file
lenDf = data.shape[0]
listNull = [[None for x in range(0, widthDf)] for y in range(lenDf)]
df_student = pd.DataFrame(listNull, columns=header)
df_student.loc[0, header] = data.loc[0, header]
j = 1
for index, row in data.iterrows():
    if index == 0:
        continue
    #if index!=data.shape[0]-1:
    if data.at[index, 'user_id'] != data.at[index - 1, 'user_id'] \
            or data.at[index, 'sore_throat_cough'] != data.at[index - 1, 'sore_throat_cough'] \
            or data.at[index, 'runnynose_congestion_sneezing'] != data.at[index - 1, 'runnynose_congestion_sneezing'] \
            or data.at[index,'fever']!= data.at[index - 1,'fever'] \
            or data.at[index,'nausea_vomiting_diarrhea']!= data.at[index - 1,'nausea_vomiting_diarrhea'] \
            or data.at[index,'sad_depressed']!= data.at[index - 1,'sad_depressed'] \
            or data.at[index,'open_stressed']!= data.at[index - 1,'open_stressed']:
        df_student.loc[j, header] = data.loc[index, header]
        j += 1
    # else:
    #     # leave the last row
    #     df_student.loc[j, header] = data.loc[index, header]
    #     j += 1
# Intercept the part with data
df_student = df_student[0:j]
print(df_student.shape)

# add startvalidtime et endvalidtime
df_student.rename(columns={'time': 'startvalidtime'},inplace=True)
endvalidtimes=df_student['startvalidtime'].copy(deep=True)
print("startvalidtime",endvalidtimes)
# remove the first row
endvalidtimes.drop(axis=0, index=0,inplace=True)
# add the last row
endvalidtimes[df_student.shape[0]]="1240610400"  #df_student.at[df_student.shape[0]-1,'startvalidtime']      #1240610400
print(type(endvalidtimes))
# minus 1 day
for i, v in endvalidtimes.iteritems():
    endvalidtimes.at[i]=int(endvalidtimes.at[i])-86400

print("endvali",endvalidtimes)
print(endvalidtimes.shape[0])
print("startvalidtime",df_student['startvalidtime'])


# label
label = ["Student"]*df_student.shape[0]
print(len(label))

# list of instancestudentid
instancesrudentid=[i for i in range(0,df_student.shape[0])]

# list of year_school
year_school=[None for i in range (0,df_student.shape[0])]
# list of floor
floor=[None for i in range (0,df_student.shape[0])]

# construct a dataframe of student
intermediate = {'instanceid:ID(Student)':instancesrudentid,'year_school':year_school,'floor':floor,':LABEL': label,'endvalidtime': endvalidtimes }
df_intermediate = DataFrame(intermediate)
df_intermediate.reset_index(drop=True, inplace=True)

# concat of 2 dataframe
df_student = pd.concat([df_intermediate, df_student], axis=1)
print(df_student)

# for each student, the endvalidtime of the last instance is equal to the startvalidtime
students= data['user_id'].unique()
for i in range(0, len(students)):
    # obtain the entity of one item
    student = df_student[df_student['user_id'].isin([students[i]])]
    listIns=student.index.tolist()
    last=listIns[-1]
    df_student.at[last,'endvalidtime']="1240610400"

# add infos of subject.csv
for index,row in df_suject.iterrows():
    df_student.loc[df_student['user_id'] == row['user_id'],'year_school']=row['year_school']
    df_student.loc[df_student['user_id'] == row['user_id'],'floor'] = row['floor']

df_student.to_csv(path[:-45]+"socialEvolution/Student_ts.csv", index=False, encoding='utf-8')
# convert timestamp to human time
df_student=convertTimeNoHMS(df_student,['startvalidtime','endvalidtime'])

# save file in Student.csv
df_student.to_csv(path[:-45]+"socialEvolution/Student.csv", index=False, encoding='utf-8')
print("successful operation")