# convert human time to timestamp and save into a file
import pandas as pd
import time
from outils import convertNeo4jTime,toTimestamp,toTimestampNoMS,convertTime,toTimestampNoHMS
st=time.time()
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/calls.csv"
#path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/receiveSMS.csv"
#path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/sendSMS.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/Proximity.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/Access.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/FluSymptoms_ts.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/BlogLivejournalTwitter.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/CloseFriend.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/PoliticalDiscussant.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/FacebookAllTaggedPhotos.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/SocializeTwicePerWeek.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/date.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/FluSymptoms_ts_student.csv"
path="C:/Users/carol/Downloads/SocialEvolution/socialEvolution/WLAN1.csv"

data=pd.read_csv(path,encoding='utf-8')

# convert timestamp
for index,row in data.iterrows():
    data.at[index,'time']=toTimestampNoHMS(data.at[index,'time'])
    data.at[index,'time']=toTimestampNoHMS(data.at[index,'time'])

    #data.at[index,'survey_date']=toTimestampNoHMS(data.at[index,'survey_date'])

# # add startvalidtime and endvalidtime
# startvalidtime=data['time'] #time
# #endvalidtime=data['time_stamp']+data['duration']
# endvalidtime=data['time'] #time
#
# validtime={'startvalidtime':startvalidtime,'endvalidtime':endvalidtime}
# df_intermediate=pd.DataFrame(validtime)
# data.drop(axis=1, columns=['time'], inplace=True) #time
# data= pd.concat([data, df_intermediate], axis=1)

data.to_csv(path[:-4]+"_ts.csv", index=False, encoding='utf-8')