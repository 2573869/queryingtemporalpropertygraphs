# transformation of file sendSMS.csv
import pandas as pd
import time
from outils import convertNeo4jTime, toTimestamp, toTimestampNoMS, convertTime

st = time.time()

path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/receiveSMS_ts_extracted.csv"  # 19 17
data = pd.read_csv(path, encoding='utf-8')

path_student="C:/Users/carol/Downloads/SocialEvolution/socialEvolution/Student_ts.csv"
df_student=pd.read_csv(path_student,encoding='utf-8')


# add column relationshiptype, instanceuser and column instancedest
instanceuserid=["" for i in range(0,data.shape[0])]
instancedestid=["" for i in range(0,data.shape[0])]
relationtype=['ReceiveSMS' for i in range(0,data.shape[0])]
instance={'START_ID':instanceuserid,'END_ID':instancedestid,':TYPE':relationtype}
df_instance=pd.DataFrame(instance)
data=pd.concat([df_instance,data],axis=1)

# add instanceid user
for index,row in data.iterrows():
    print(index)
    indexusers = df_student[
        (row['user_id'] == df_student.user_id) & (df_student.startvalidtime <= row['startvalidtime']) & (
                    df_student.endvalidtime+86399 >= row['endvalidtime'])].index.tolist()
    indexremote = df_student[(row['dest_user_id_if_known'] == df_student.user_id) & (
                df_student.startvalidtime <= row['startvalidtime']) & (
                                         df_student.endvalidtime+86399 >= row['endvalidtime'])].index.tolist()
    if (len(indexusers) == 1 & len(indexremote) == 1):
        userid = indexusers[0]
        removeid = indexremote[0]
        data.at[index, 'START_ID'] = removeid
        data.at[index, 'END_ID'] = userid

# remove the rows where the column validtime is empty
data=data[(data.START_ID!="")|(data.END_ID!="")]

# convert timestamp to human time
data=convertTime(data,['startvalidtime','endvalidtime'])

# rename
data.rename(columns={'START_ID': ':START_ID(Student)','END_ID':':END_ID(Student)'},inplace=True)

# save the file
data.to_csv(path[:-46]+"socialEvolution/ReceiveSMS1.csv", index=False, encoding='utf-8')

et = time.time()
cost_time = et - st
print('cost time：{}seconds'.format(float('%.2f' % cost_time)))