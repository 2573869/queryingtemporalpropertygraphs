# transformation for Proximity.csv
import pandas as pd
import time
import os
from outils import convertNeo4jTime, toTimestamp, toTimestampNoMS, convertTime
path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/pro"

files = os.listdir(path)
csv_list = []
for f in files:
    if os.path.splitext(f)[1] == '.csv':
        csv_list.append(path + '\\' + f)
    else:
        pass
ligne = 0
path_student = "C:/Users/carol/Downloads/SocialEvolution/socialEvolution/Student_ts.csv"
df_student = pd.read_csv(path_student, encoding='utf-8')
for file in range(0, len(csv_list)):
    st = time.time()
    print("start ", file, " file")
    st = time.time()
    #path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/p.csv"
    # path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/Proximity.csv"
    data = pd.read_csv(csv_list[file], encoding="utf-8")


    # # convert timestamp
    # for index,row in data.iterrows():
    #     data.at[index,'time']=toTimestampNoMS(data.at[index,'time'])
    #
    # # add startvalidtime and endvalidtime
    # startvalidtime=data['time']
    # endvalidtime=data['time']
    # validtime={'startvalidtime':startvalidtime,'endvalidtime':endvalidtime}
    # df_intermediate=pd.DataFrame(validtime)
    # data.drop(axis=1, columns=['time'], inplace=True)
    # data= pd.concat([data, df_intermediate], axis=1)
    #
    # data.to_csv(path[:-4]+"_ts.csv", index=False, encoding='utf-8')

    # add column instance
    instanceuser = ["" for i in range(0, data.shape[0])]
    instanceremove = ["" for i in range(0, data.shape[0])]
    relationtype = ['Proximity' for i in range(0, data.shape[0])]
    instance = {'START_ID': instanceuser, 'END_ID': instanceremove, ':TYPE': relationtype}
    df_instance = pd.DataFrame(instance)
    data = pd.concat([df_instance, data], axis=1)
    # add instanceid
    for index, row in data.iterrows():
        print(index)
        indexusers = df_student[
            (row['user_id'] == df_student.user_id) & (df_student.startvalidtime <= row['startvalidtime']) & (
                        df_student.endvalidtime + 86399 >= row['endvalidtime'])].index.tolist()
        indexremote = df_student[(row['remote_user_id_if_known'] == df_student.user_id) & (
                    df_student.startvalidtime <= row['startvalidtime']) & (
                                             df_student.endvalidtime + 86399 >= row['endvalidtime'])].index.tolist()
        # if (len(indexusers)==0|len(indexremote)==0):
        #     data.drop(index=index,inplace=True,axis=0)
        #     print(index,"drop")
        if (len(indexusers) == 1 & len(indexremote) == 1):
            userid = indexusers[0]
            removeid = indexremote[0]
            data.at[index, 'START_ID'] = userid
            data.at[index, 'END_ID'] = removeid

    # remove the rows where the column validtime is empty
    data = data[(data.START_ID != "") & (data.END_ID != "")]
    data.round({'user_id': 0, 'remote_user_id_if_known': 0})

    # convert ts to neo4j time
    data = convertTime(data, ['startvalidtime'])
    data.drop(['endvalidtime'], axis=1, inplace=True)
    endvalidtimes = data['startvalidtime'].copy(deep=True)

    # construct a dataframe
    intermediate = {'endvalidtime': endvalidtimes}
    df_intermediate = pd.DataFrame(intermediate)


    # concat of 2 dataframe
    data = pd.concat([data, df_intermediate], axis=1)
    # rename
    data.rename(columns={'START_ID': ':START_ID(Student)', 'END_ID': ':END_ID(Student)'}, inplace=True)

    # save file
    data.to_csv("C:/Users/carol/Downloads/SocialEvolution/intermediate_files/pro/pf/Proximity_" +str(file+1)+ ".csv", index=False, encoding='utf-8')  # 49
    # data.to_csv(path[:-4]+"_test.csv", index=False, encoding='utf-8')

    et = time.time()
    cost_time = et - st
    print('cost time：{}seconds'.format(float('%.2f' % cost_time)))
